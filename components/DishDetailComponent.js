import React, { Component } from 'react';
import { Text, View, ScrollView, FlatList, StyleSheet, Button, Modal, Alert, PanResponder, Share} from 'react-native';
import TextInput from 'react-native-textinput-with-icons';
import { Card, Icon, Rating } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { postFavorite, postComment, fetchComments } from '../redux/ActionCreators';

import * as Animatable from 'react-native-animatable';

const mapStateToProps = state => {
    return {
      dishes: state.dishes,
      comments: state.comments,
      favorites: state.favorites
    }
  }

const mapDispatchToProps = dispatch => ({
    postFavorite: (dishId) => dispatch(postFavorite(dishId)),
    postComment: (dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment))
})

function RenderComments(props) {

    const comments = props.comments;
            
    const renderCommentItem = ({item, index}) => {
        
        return (
            <View key={index} style={{margin: 10}}>
                <Text style={{fontSize: 14}}>{item.comment}</Text>
                <Text style={{fontSize: 12}}>{item.rating} Stars</Text>
                <Text style={{fontSize: 12}}>{'-- ' + item.author + ', ' + item.date} </Text>
            </View>
        );
    };
    
    return (
        <Animatable.View animation="fadeInUp" duration={2000} delay={1000}>    
            <Card title='Comments' >
                <FlatList 
                    data={comments}
                    renderItem={renderCommentItem}
                    keyExtractor={(item, index) => index.toString()}
                    />
            </Card>
        </Animatable.View>
    );
}

class Dishdetail extends Component {

    constructor(props) {
        super(props);
       
        this.state = {
            favorites: [],
            isModalOpen: false,
            rating: 0,
            author: '',
            comment: ''
        };
    }

    static navigationOptions = {
        title: 'Dish Details'
    };

    toggleForm(){
        this.setState({
            isModalOpen: !this.state.isModalOpen
        });
    }

    handleCommentSubmit(){
        const dishId = this.props.navigation.getParam('dishId','');
        console.log(JSON.stringify(this.state));
        this.props.postComment(dishId, this.state.rating, this.state.author, this.state.comment);
        this.toggleForm();
    }

    markFavorite(dishId) {
        this.props.postFavorite(dishId);
    }

    RenderForm = RenderForm = () => {
        
        return(
        <View>
        <Modal animationType = {"slide"} transparent = {false}
            visible = {this.state.isModalOpen}
            onDismiss = {() => this.toggleForm() }
            onRequestClose = {() => this.toggleForm() }>
            <View style = {styles.modal}>
                <Rating showRating fractions={1} startingValue={0}
                    onFinishRating={(rating) => this.setState({rating})}/>
                <View>
                    <TextInput 
                        onChangeText={(value) => { this.setState({author:value}); console.log(this.state.author);} }
                        label='Author'
                        leftIcon="md-person"
                        leftIconSize={30}
                        />
                    <TextInput
                        onChangeText={(value) => { this.setState({comment:value}); console.log(this.state.comment);} }
                        label='Comment'
                        leftIcon="md-chatbubbles"
                        leftIconSize={30}
                        />
                </View>
                <View style={styles.buttonSection}>
                    <View style={styles.Button}>
                        <Button 
                            onPress={() => this.handleCommentSubmit()}
                            color="#6918bf"
                            title="SUBMIT"
                            />
                    </View>
                   <View style={styles.Button}>
                        <Button 
                            onPress = {() => {this.toggleForm()} }
                            color="#adadad"
                            title="CANCEL"
                            />
                   </View>
                </View>
            </View>
        </Modal>
        </View>
        );
    }

    render() {
        
        const Button = <CommentButton onPress={ () => this.toggleForm() } />;
        const dishId = this.props.navigation.getParam('dishId','');
        const comments = this.props.comments.comments.filter((comment) => comment.dishId === dishId);
        if(comments != null || typeof(comments) !== 'undefinied'){
        return(
            <ScrollView>
                <RenderDish dish={this.props.dishes.dishes[+dishId]}
                    favorite={this.props.favorites.some(el => el === dishId)}
                    onPress={() => this.markFavorite(dishId)}
                    commentButton={Button}
                    toggleForm={() => this.toggleForm()}
                    />
                <RenderComments comments={comments} />
                <RenderForm />
            </ScrollView>
        );
    }else{
        return(
            <View>
                <Text>No comments to show now</Text>
            </View>
        );
    } 

    }
}

function CommentButton(props){

    return(
        <Icon
            raised
            reverse
            name='pencil'
            type='font-awesome'
            color='#ca1dd3'
            onPress={() => props.onPress()}
            />
    );
}

function RenderDish(props) {

    const dish = props.dish;

    handleViewRef = ref => this.view = ref;

    const recognizeDrag = ({ moveX, moveY, dx, dy }) => {
        if ( dx < -200 )
            return true;
        else (dx < -400)
            return false;
    };

    const recognizeDragLeft = ({ dx }) => {
        if (dx > 200)
            return true;
        else
            return false;
    };
    
    const panResponder = PanResponder.create({
        onStartShouldSetPanResponder: (e, gestureState) => {
            return true;
        },
        onPanResponderGrant: () => {this.view.rubberBand(1000)
            .then(endState => console.log(endState.finished ? 'finished' : 'cancelled'));
        },
        onPanResponderEnd: (e, gestureState) => {
            console.log("pan responder end", gestureState);
            if (recognizeDrag(gestureState)){
                Alert.alert(
                    'Add Favorite',
                    'Are you sure you wish to add ' + dish.name + ' to favorite?',
                    [
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'OK', onPress: () => {props.favorite ? console.log('Already favorite') : props.onPress()}},
                    ],
                    { cancelable: false }
                );
            }else if(recognizeDragLeft(gestureState)){
                props.toggleForm();
            }
            return true;
        }
    });

    const shareDish = (title, message, url) => {
        Share.share({
            title: title,
            message: title + ': ' + message + ' ' + url,
            url: url
        },{
            dialogTitle: 'Share ' + title
        })
    };

        if (dish != null) {
            return(
                <View>
                    <Animatable.View 
                        animation="fadeInDown" 
                        duration={2000} delay={1000} 
                        {...panResponder.panHandlers} 
                        ref={this.handleViewRef}>
                        <Card
                        featuredTitle={dish.name}
                        image={{uri: baseUrl + dish.image}}>
                            <Text style={{margin: 10}}>
                                {dish.description}
                            </Text>
                            <View style={styles.container}>
                                <Icon
                                    raised
                                    reverse
                                    name={ props.favorite ? 'heart' : 'heart-o'}
                                    type='font-awesome'
                                    color='#f50'
                                    onPress={() => props.favorite ? console.log('Already favorite') : props.onPress()}
                                    />
                                {props.commentButton}
                                <Icon
                                    raised
                                    reverse
                                    name='share'
                                    type='font-awesome'
                                    color='#51D2A8'
                                    style={styles.cardItem}
                                    onPress={() => shareDish(dish.name, dish.description, baseUrl + dish.image)} 
                                    />
                            </View>
                        </Card>
                    </Animatable.View>
                </View>
            );
        }
        else {
            return(<View></View>);
        }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row'
    },
    modal: {
        justifyContent: 'flex-start',
        flex: 1,
        margin: 20
    },
    inputSection:{
        flex:1,
        flexDirection: 'column',
        margin: 20
    },
    buttonSection: {
        flex: 1,
        height: 100,
        flexDirection: 'column',
        margin: 20
    },
    button:{
        flex: 1,
        marginBottom: 10
    }
});


export default connect(mapStateToProps, mapDispatchToProps)(Dishdetail);